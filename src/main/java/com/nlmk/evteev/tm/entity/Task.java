package com.nlmk.evteev.tm.entity;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import javax.xml.bind.annotation.XmlElement;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Класс, описывающий задачи
 */
@JacksonXmlRootElement(localName = "Project")
public class Task implements Serializable {

    private static final long serialVersionUID = 22L;

    @JacksonXmlProperty(localName = "taskId")
    private Long id = System.nanoTime();
    @JacksonXmlProperty(localName = "taskName")
    private String name;
    @JacksonXmlProperty(localName = "taskDescription")
    private String description;
    @JacksonXmlProperty()
    private Long projectId;
    @JacksonXmlProperty()
    private LocalDateTime deadLine;

    /**
     * Конструктор класса
     *
     * @param pName название задачи
     */
    public Task(String pName) {
        name = pName;
    }

    public Long getId() {
        return id;
    }

    @XmlElement
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    @XmlElement
    public void setDescription(String description) {
        this.description = description;
    }

    public Long getProjectId() {
        return projectId;
    }

    @XmlElement
    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        return "Task {id = " + id + ",\n name = " + name
                + ",\n projectId = " + projectId
                + "\n, deadLine = " + deadLine.toString() + "}";
    }


    public LocalDateTime getDeadLine() {
        return deadLine;
    }

    public void setDeadLine(LocalDateTime deadLine) {
        this.deadLine = deadLine;
    }
}
