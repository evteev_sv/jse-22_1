package com.nlmk.evteev.tm.entity;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Класс, описывающий проект
 */
@JacksonXmlRootElement(localName = "Project")
public class Project implements Serializable {

    private static final long serialVersionUID = 21L;
    @JacksonXmlElementWrapper(localName = "tasks")
    @JacksonXmlProperty(localName = "task")
    private final List<Long> tasks = new ArrayList<>();
    @JacksonXmlProperty()
    private Long id = System.nanoTime();
    @JacksonXmlProperty()
    private String name = "";
    @JacksonXmlProperty()
    private String description = "";
    @JacksonXmlProperty(localName = "ownedUser")
    private UUID ownedUser;

    /**
     * Создание класса
     *
     * @param pName имя проекта
     */
    public Project(String pName) {
        name = pName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Long> getTasks() {
        return tasks;
    }

    public UUID getOwnedUser() {
        return ownedUser;
    }

    public void setOwnedUser(UUID ownedUser) {
        this.ownedUser = ownedUser;
    }

    @Override
    public String toString() {
        return "Project {id=" + id + ",\n name=" + name + "}\n";
    }

}
