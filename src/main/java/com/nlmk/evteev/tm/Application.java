package com.nlmk.evteev.tm;

import com.nlmk.evteev.tm.enumerations.UserRole;
import com.nlmk.evteev.tm.repository.TaskRepository;
import com.nlmk.evteev.tm.repository.UserRepository;
import com.nlmk.evteev.tm.service.TaskService;
import com.nlmk.evteev.tm.subscriber.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Calendar;

/**
 * Основной класс
 */
public class Application {

    private static final Logger logger = LogManager.getLogger(Application.class);

    static {
        new UserRepository().create("ivanov", "qwerty", UserRole.ADMIN,
                "Иванов", "Иван", "Иванович");
        new UserRepository().create("petrov", "asdzxc", UserRole.USER,
                "Петров", "Петр", "Петрович");

    }

    /**
     * Точка входа
     *
     * @param args дополнительные аргументы запуска
     */
    public static void main(String[] args) {
        logger.debug("Start application at " + Calendar.getInstance().getTime());
        SystemSubscriber systemSubscriber = new SystemSubscriber();
        Publisher.getInstance().addSubscriber(systemSubscriber);
        UserSubscriber userSubscriber = new UserSubscriber();
        Publisher.getInstance().addSubscriber(userSubscriber);
        TaskSubscriber taskSubscriber = new TaskSubscriber(new TaskService(new TaskRepository()));
        if (args != null && args.length > 0 && args[0] != null && args[0].equals("notify=y")) {
            taskSubscriber.startNotify();
        }
        Publisher.getInstance().addSubscriber(taskSubscriber);
        ProjectSubscriber projectSubscriber = new ProjectSubscriber();
        Publisher.getInstance().addSubscriber(projectSubscriber);
        Publisher.getInstance().initPublisher();
        logger.debug("Stop application at" + Calendar.getInstance().getTime());
    }

}
