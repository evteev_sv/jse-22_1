package com.nlmk.evteev.tm.subscriber;

import com.nlmk.evteev.tm.repository.UserRepository;
import com.nlmk.evteev.tm.service.SystemService;
import com.nlmk.evteev.tm.service.UIService;
import com.nlmk.evteev.tm.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

import static com.nlmk.evteev.tm.constant.TerminalConst.CMD_EXIT;

/**
 * Класс издателя для посылки комманд подписчикам
 */
public class Publisher {

    private static final Logger logger = LogManager.getLogger(Publisher.class);
    private static Publisher instance;
    protected final Scanner scanner = new Scanner(System.in);
    private final UserRepository userRepository = new UserRepository();
    private final UserService userService = UserService.getInstance(userRepository);
    private final List<ISubscriber> subscribers;

    private Publisher() {
        subscribers = new ArrayList<>();
    }

    /**
     * Создание и получение экземпляра издателя
     *
     * @return экземпляр издателя {@link Publisher}
     */
    public static Publisher getInstance() {
        synchronized (Publisher.class) {
            return instance == null
                    ? instance = new Publisher()
                    : instance;
        }
    }

    /**
     * Инициализация издателя
     */
    public void initPublisher() {
        String command = "";
        Map<String, String> params = new HashMap<>();
        UIService.getInstance().doEnterUserLogin(params);
        UIService.getInstance().doEnterUserPassword(params);
        userService.userLogin(params);
        SystemService.getInstance().displayWelcome(userService.getAppUser());
        run("proj-import-xml");
        while (!CMD_EXIT.equals(command)) {
            command = scanner.nextLine();
            run(command);
        }
    }

    /**
     * Добавление подписчика на сообщения
     *
     * @param subscriber подписчик, реализующий интерфейс {@link ISubscriber}
     */
    public void addSubscriber(ISubscriber subscriber) {
        if (!subscribers.contains(subscriber)) {
            subscribers.add(subscriber);
            logger.info(subscriber.toString(), () -> "Добавлен подписчик {}");
        }
    }

    /**
     * Удаление подписчика на сообщения
     *
     * @param subscriber подписчик, реализующий интерфейс {@link ISubscriber}
     */
    public void deleteSubscriber(ISubscriber subscriber) {
        if (subscribers.remove(subscriber)) {
            logger.info(subscriber.toString(), () -> "Удален подписчик {}");
        } else {
            logger.warn(subscriber.toString(), () -> "Удаление подписчика не удачно. В списке нет такого подписчика: {}");
        }
    }

    /**
     * Основной метод исполнения
     *
     * @param message команда
     */
    private void run(final String message) {
        if (message == null || message.isEmpty()) {
            logger.warn("Команда пустая! Выполнение прервано.");
            return;
        }
        SystemService.getInstance().addToHistory(message);
        subscribers.forEach(iSubscriber -> iSubscriber.handleNotify(message));
    }

}
