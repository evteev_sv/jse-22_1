package com.nlmk.evteev.tm.subscriber;


import com.nlmk.evteev.tm.exceptions.TaskNotFoundException;
import com.nlmk.evteev.tm.service.TaskService;
import com.nlmk.evteev.tm.service.UIService;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

public class TaskSubscriber implements ISubscriber {

    private final UIService uiService = UIService.getInstance();
    private final TaskService taskService;
    private final Logger logger = LogManager.getLogger(TaskSubscriber.class);

    public TaskSubscriber(TaskService taskService) {
        this.taskService = taskService;
    }

    public void startNotify() {
        TaskManagerUtil.hasRemoveOldestTaskFlag = true;
        taskService.notifyTaskDeadLine();
        TaskManagerUtil.hasNotifyDeadLineFlag = true;
        taskService.removeOldTask();
    }


    @Override
    public void handleNotify(String msg) {
        if (msg == null || msg.isEmpty()) {
            logger.warn("Команда пустая. Выполнение прервано.");
            return;
        }
        Map<String, String> params = new HashMap<>();
        switch (msg) {
            case TASK_CREATE:
                logger.info("[CREATE TASK]");
                uiService.doEnterTaskName(params);
                uiService.doEnterTaskDeadLine(params);
                taskService.createTask(params);
                break;
            case TASK_CLEAR:
                logger.info("[CLEAR TASK]");
                taskService.clearTask();
                break;
            case TASK_LIST:
                logger.info("[LIST TASK]");
                taskService.listTask();
                break;
            case TASK_VIEW:
                uiService.doEnterTaskIndex(params);
                taskService.viewTaskByIndex(params);
                break;
            case TASK_VIEW_WITHOUT_PROJECT:
                logger.info("[VIEW TASKS WITHOUT PROJECT]");
                try {
                    taskService.findAllWithoutProject();
                } catch (TaskNotFoundException ne) {
                    logger.error(ne.getMessage());
                }
                break;
            case TASK_VIEW_BY_PROJECT:
                logger.info("[VIEW TASKS BY PROJECT ID]");
                uiService.doEnterProjectId(params);
                try {
                    taskService.findAllByProjectId(params);
                } catch (TaskNotFoundException ne) {
                    logger.error(ne.getMessage());
                }
                break;
            case TASK_REMOVE_BY_ID:
                logger.info("[Remove task by id]");
                uiService.doEnterTaskId(params);
                try {
                    taskService.removeTaskById(params);
                } catch (TaskNotFoundException te) {
                    logger.error(te.getMessage());
                }
                break;
            case TASK_REMOVE_BY_NAME:
                logger.info("[Remove task by name]");
                uiService.doEnterTaskName(params);
                try {
                    taskService.removeTaskByName(params);
                } catch (TaskNotFoundException te) {
                    logger.error(te.getMessage());
                }
                break;
            case TASK_REMOVE_BY_INDEX:
                logger.info("[Remove task by index]");
                uiService.doEnterTaskIndex(params);
                try {
                    taskService.removeTaskByIndex(params);
                } catch (TaskNotFoundException te) {
                    logger.error(te.getMessage());
                }
                break;
            case TASK_UPDATE_BY_ID:
                logger.info("[Update task by id]");
                uiService.doEnterTaskId(params);
                uiService.doEnterNewTaskNameAndDescription(params);
                try {
                    taskService.updateTaskById(params);
                } catch (TaskNotFoundException ne) {
                    logger.error(ne.getMessage());
                }
                break;
            case TASK_UPDATE_BY_INDEX:
                logger.info("[Update task by index]");
                uiService.doEnterTaskIndex(params);
                uiService.doEnterNewTaskNameAndDescription(params);
                taskService.updateTaskByIndex(params);
                break;
            default:
                break;
        }
    }

}
