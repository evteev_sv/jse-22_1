package com.nlmk.evteev.tm.subscriber;

import com.nlmk.evteev.tm.enumerations.ExportType;
import com.nlmk.evteev.tm.exceptions.ProjectNotFoundException;
import com.nlmk.evteev.tm.exceptions.TaskNotFoundException;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.service.ProjectService;
import com.nlmk.evteev.tm.service.ProjectTaskService;
import com.nlmk.evteev.tm.service.UIService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.AccessDeniedException;
import java.util.HashMap;
import java.util.Map;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

public class ProjectSubscriber implements ISubscriber {

    private static final Logger logger = LogManager.getLogger(ProjectSubscriber.class);
    private final UIService uiService = UIService.getInstance();
    private final ProjectService projectService = new ProjectService(new ProjectRepository());

    @Override
    public void handleNotify(String msg) {
        if (msg == null || msg.isEmpty()) {
            logger.warn("Команда пустая. Выполнение прервано.");
            return;
        }
        Map<String, String> params = new HashMap<>();
        switch (msg) {
            case PROJECT_CREATE:
                logger.info("[CREATE PROJECT]");
                uiService.doEnterProjectName(params);
                projectService.createProject(params);
                break;
            case PROJECT_CLEAR:
                logger.info("[CLEAR PROJECT]");
                projectService.clearProject();
                break;
            case PROJECT_LIST:
                logger.info("[LIST PROJECT]");
                projectService.listProject();
                break;
            case PROJECT_VIEW:
                    uiService.doEnterProjectIndex(params);
                    projectService.viewProjectByIndex(params);
                break;
            case PROJECT_VIEW_WITH_TASKS:
                logger.info("[VIEW PROJECT WITH TASKS]");
                try {
                    ProjectTaskService.getInstance().viewProjectWithTasks();
                } catch (ProjectNotFoundException | AccessDeniedException e) {
                    logger.error("Ошибка вывода проекта! ".concat(e.getMessage()));
                }
                break;
            case PROJECT_REMOVE_BY_ID:
                logger.info("[Remove project by id]");
                uiService.doEnterProjectId(params);
                try {
                    projectService.removeProjectById(params);
                } catch (ProjectNotFoundException | AccessDeniedException pe) {
                    logger.error(pe.getMessage());
                }
                break;
            case PROJECT_REMOVE_BY_NAME:
                logger.info("[Remove project by name]");
                uiService.doEnterProjectName(params);
                try {
                    projectService.removeProjectByName(params);
                } catch (AccessDeniedException | ProjectNotFoundException e) {
                    logger.error(e.getMessage());
                }
                break;
            case PROJECT_REMOVE_BY_INDEX:
                logger.info("[Remove project by index]");
                try {
                    uiService.doEnterProjectIndex(params);
                    projectService.removeProjectByIndex(params);
                } catch (ProjectNotFoundException | AccessDeniedException e) {
                    logger.error("Удаление проекта не удалось! ".concat(e.getMessage()));
                }
                break;
            case PROJECT_UPDATE_BY_INDEX:
                logger.info("[Update project by index]");
                try {
                    uiService.doEnterProjectIndex(params);
                    uiService.doEnterProjectName(params);
                    uiService.doEnterProjectDescription(params);
                    projectService.updateProjectByIndex(params);
                } catch (ProjectNotFoundException e) {
                    logger.error("Обновление проекта не удалось! ".concat(e.getMessage()));
                }
                break;
            case PROJECT_UPDATE_BY_ID:
                logger.info("[Update project by id]");
                uiService.doEnterProjectId(params);
                uiService.doEnterProjectName(params);
                uiService.doEnterProjectDescription(params);
                try {
                    projectService.updateProjectById(params);
                } catch (ProjectNotFoundException e) {
                    logger.error(e.getMessage());
                }
                break;
            case PROJECT_ADD_TASK_BY_IDS:
                logger.info("[ADD TASK TO PROJECT BY IDS]");
                try {
                    ProjectTaskService.getInstance().addTaskToProjectByIds();
                } catch (ProjectNotFoundException | TaskNotFoundException | AccessDeniedException e) {
                    logger.error("Ошибка добавления задачи! ".concat(e.getMessage()));
                }
                break;
            case PROJECT_REMOVE_TASK_BY_IDS:
                logger.info("[REMOVE TASK FROM PROJECT BY IDS]");
                try {
                    ProjectTaskService.getInstance().removeTaskFromProjectById();
                } catch (ProjectNotFoundException | TaskNotFoundException | AccessDeniedException e) {
                    logger.error("Ошибка удаления задачи! ".concat(e.getMessage()));
                }
                break;
            case PROJECT_REMOVE_TASKS:
                logger.info("[CLEAR PROJECT TASKS]");
                try {
                    ProjectTaskService.getInstance().clearProjectTasks();
                } catch (ProjectNotFoundException | AccessDeniedException e) {
                    logger.error(e.getMessage());
                }
                break;
            case PROJECT_REMOVE_WITH_TASKS:
                logger.info("[REMOVE PROJECT WITH TASKS]");
                try {
                    ProjectTaskService.getInstance().removeProjectWithTasks();
                } catch (ProjectNotFoundException | AccessDeniedException e) {
                    logger.error(e.getMessage());
                }
                break;
            case PROJECT_CHANGE_OWNER:
                uiService.doEnterProjectId(params);
                uiService.doEnterUserCode(params);
                try {
                    projectService.assignProjectToUser(params);
                } catch (AccessDeniedException | ProjectNotFoundException e) {
                    logger.error(e.getMessage());
                }
                break;
            case PROJECT_EXPORT_JSON:
                ProjectTaskService.getInstance().exportData(ExportType.TYPE_JSON);
                break;
            case PROJECT_EXPORT_XML:
                ProjectTaskService.getInstance().exportData(ExportType.TYPE_XML);
                break;
            case PROJECT_IMPORT_JSON:
                ProjectTaskService.getInstance().importData(ExportType.TYPE_JSON);
                break;
            case PROJECT_IMPORT_XML:
                ProjectTaskService.getInstance().importData(ExportType.TYPE_XML);
                break;
            default:
                break;
        }
    }

}
