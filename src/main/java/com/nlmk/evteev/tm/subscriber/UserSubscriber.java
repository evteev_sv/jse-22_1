package com.nlmk.evteev.tm.subscriber;

import com.nlmk.evteev.tm.repository.UserRepository;
import com.nlmk.evteev.tm.service.UIService;
import com.nlmk.evteev.tm.service.UserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

/**
 * Класс-подписчик на команды издателя {@link Publisher}
 * <p>
 * Обрабатывает команды, касающиеся пользователей
 * <p/>
 */
public class UserSubscriber implements ISubscriber {

    private static final Logger logger = LogManager.getLogger(UserSubscriber.class);
    private final UIService uiService = UIService.getInstance();
    private final UserRepository userRepository = new UserRepository();
    private final UserService userService = UserService.getInstance(userRepository);

    @Override
    public void handleNotify(String msg) {
        if (msg == null || msg.isEmpty()) {
            logger.warn("Команда пустая, выполнение прервано.");
            return;
        }
        Map<String, String> params = new HashMap<>();
        switch (msg) {
            case USER_CREATE:
                userService.createUser(uiService.doUserCreate());
                break;
            case USER_UPDATE:
                userService.updateUser(uiService.doUserUpdate(true));
                break;
            case USER_DELETE:
                uiService.doEnterUserLogin(params);
                userService.deleteUser(params);
                break;
            case USER_LIST:
                userService.printListUsers();
                break;
            case USER_LOGIN:
                uiService.doEnterUserLogin(params);
                uiService.doEnterUserPassword(params);
                userService.userLogin(params);
                break;
            case USER_VIEW:
                uiService.doEnterUserCode(params);
                userService.viewUserProfile(params);
                break;
            case USER_CHANGE_PASSWORD:
                uiService.doEnterUserCode(params);
                uiService.doEnterUserNewPassword(params);
                userService.changeUserPassword(params);
                break;
            case USER_PROFILE_EDIT:
                params = uiService.doUserUpdate(false);
                uiService.doEnterUserCode(params);
                userService.editUserProfile(params);
                break;
            case USER_LOGOUT:
                userService.userLogOut();
                break;
            default:
                break;
        }
    }

}
