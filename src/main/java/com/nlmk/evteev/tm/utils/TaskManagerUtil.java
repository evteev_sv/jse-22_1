package com.nlmk.evteev.tm.utils;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.repository.UserRepository;
import com.nlmk.evteev.tm.service.UserService;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Класс-утилита
 */
public class TaskManagerUtil {

    private static final UserService userService = UserService.getInstance(new UserRepository());
    private static final ProjectRepository projectRepository = new ProjectRepository();
    public static boolean hasNotifyDeadLineFlag = true;
    public static boolean hasRemoveOldestTaskFlag = true;

    private TaskManagerUtil() {

    }

    /**
     * Получение hash пароля
     *
     * @param str пароль
     * @return сроковое представление hash
     */
    public static String getMD5Hash(final String str) {
        if (str == null || str.isEmpty()) return "";
        MessageDigest md;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(str.getBytes());
            return new String(md.digest());
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }

    /**
     * Проверка вводимого значения на NULL или пустое значение.
     *
     * @param inputString вводимая строка
     * @return истина, если строка не пустая, иначе ложь
     */
    public static boolean checkEmptyInput(final String inputString) {
        return inputString != null && !inputString.isEmpty();
    }

    /**
     * Проверка на привилегий на проект
     *
     * @param project проект {@link Project}
     * @return истина, если проект принадлежит пользователю, или пользователь является администратором,
     * иначе - ложь
     */
    public static boolean checkProjectPrivs(final Project project) {
        if (project == null) {
            return false;
        }
        if (project.getOwnedUser() == null) {
            return userService.getAppUser().isAdmin();
        }
        if (project.getOwnedUser() != null) {
            return project.getOwnedUser().equals(userService.getAppUser().getUserId())
                    || userService.getAppUser().isAdmin();
        }
        return false;
    }

    public static boolean checkTaskPrivs(final Task task) {
        if (task == null) {
            return false;
        }
        Project project = projectRepository.findById(task.getId());
        return project != null && project.getOwnedUser().equals(userService.getAppUser().getUserId());
    }

    /**
     * Вывод сообщения о неверных действиях
     *
     * @param whatPrint текст сообщения
     */
    public static void printAndReturnFail(final String whatPrint) {
        System.out.println(whatPrint);
        System.out.println("[FAIL]");
    }

    /**
     * Вывод безошибочного сообщения
     */
    public static void printAndReturnOk() {
        System.out.println("[OK]");
    }

}
