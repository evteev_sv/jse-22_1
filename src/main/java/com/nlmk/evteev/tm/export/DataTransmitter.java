package com.nlmk.evteev.tm.export;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nlmk.evteev.tm.enumerations.ExportType;
import com.nlmk.evteev.tm.repository.AbstractRepository;

import java.io.*;
import java.nio.charset.Charset;
import java.util.List;

public class DataTransmitter<T> extends AbstractTransmitter<T> {

    public DataTransmitter(ExportType exportType, AbstractRepository<T> repository) {
        super(exportType, repository);
    }

    /**
     * Основная процедура экспорта данных
     *
     * @param fileName имя файла, в который производится экспорт
     * @throws IOException ошибка при экспорте
     */
    @Override
    public void exportToFile(String fileName) throws IOException {
        File file = new File(FILE_PATH.concat(fileName));
        if (!file.exists()) {
            file.getParentFile().mkdir();
        }
        List<T> list = getAbstractRepository().findAll();
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            switch (getExportType()) {
                case TYPE_JSON:
                    Gson gson = new Gson();
                    byte[] strToBytes = gson.toJson(list).getBytes(Charset.defaultCharset());
                    outputStream.write(strToBytes);
                    break;
                case TYPE_XML:
                    XmlMapper xmlMapper = new XmlMapper();
                    String str = xmlMapper.writeValueAsString(list);
                    byte[] bytes = xmlMapper.writeValueAsString(list).getBytes(Charset.defaultCharset());
                    outputStream.write(bytes);
                    break;
            }
        }
    }

    /**
     * Процедура импорта данных
     *
     * @param fileName имя файла, в который производится экспорт
     * @throws FileNotFoundException ошибка при импорте
     */
    @Override
    public void importFromFile(String fileName) throws IOException {
        File file = new File(FILE_PATH);
        FilenameFilter filenameFilter = (File dir, String name) ->
                getExportType() == ExportType.TYPE_JSON
                        ? name.endsWith(".json")
                        : name.endsWith(".xml");
        File[] files = file.listFiles(filenameFilter);
        if (files.length == 0) {
            throw new FileNotFoundException("Файлы данных не найдены.");
        }
        for (File f : files) {
            String dataString;
            dataString = getStringData(f);
            switch (getExportType()) {
                case TYPE_JSON:
                    Gson gson = new Gson();
                    getAbstractRepository().updateStorage(gson.fromJson(dataString, new TypeToken<List<T>>() {
                    }.getType()));
                    break;
                case TYPE_XML:
                    XmlMapper xmlMapper = new XmlMapper();
                    List<T> list = xmlMapper.readValue(dataString, new TypeReference<List<T>>() {
                    });
                    getAbstractRepository().updateStorage(list);
                    break;
            }
        }
    }

    /**
     * Получение данных из файла
     *
     * @param file файл, с которого происходит считывание
     * @return Строка данных
     * @throws IOException ошибка при считывании данных
     */
    private String getStringData(File file) throws IOException {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
            return stringBuilder.toString();
        }
    }

}
