package com.nlmk.evteev.tm.export;

import com.nlmk.evteev.tm.enumerations.ExportType;
import com.nlmk.evteev.tm.repository.AbstractRepository;

import java.io.IOException;

public abstract class AbstractTransmitter<T> {

    protected static final String FILE_PATH = "data/";
    private final AbstractRepository<T> abstractRepository;
    private final ExportType exportType;

    public AbstractTransmitter(ExportType exportType, AbstractRepository<T> repository) {
        this.exportType = exportType;
        this.abstractRepository = repository;
    }

    public ExportType getExportType() {
        return exportType;
    }

    public AbstractRepository<T> getAbstractRepository() {
        return abstractRepository;
    }

    public abstract void exportToFile(String fileName) throws IOException;

    public abstract void importFromFile(String fileName) throws IOException;


}
