package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.exceptions.ProjectNotFoundException;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.repository.UserRepository;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;

import java.nio.file.AccessDeniedException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

/**
 * Сервисный класс для работы с репозиторием проектов
 */
public class ProjectService {


    private final ProjectRepository projectRepository;
    private final UserRepository userRepository = new UserRepository();
    private final UserService userService = UserService.getInstance(userRepository);

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /**
     * Создание проекта и добавление его в список проектов
     *
     * @param name   имя проекта
     * @param userId код пользователя {@link UUID}
     * @return объект типа {@link Project}
     */
    private Optional<Project> create(final String name, final UUID userId) {
        if (name == null || name.isEmpty()) {
            return Optional.empty();
        }
        return Optional.ofNullable(projectRepository.create(name, userId));
    }

    /**
     * Очистка списка проектов
     */
    private void clear() {
        projectRepository.clear();
    }

    /**
     * Очистка списка на основании другого списка проектов
     *
     * @param projectList список проектов для очистки {@link List}
     */
    private void clearListProject(List<Project> projectList) {
        projectRepository.clearListProject(projectList);
    }

    /**
     * Возвращает список проектов
     *
     * @return список проектов {@link List}
     */
    private List<Project> findAll() {
        return projectRepository.findAll();
    }

    /**
     * Поиск проекта по индексу
     *
     * @param index индекс проекта
     * @return проект {@link Project}
     */
    private Optional<Project> findByIndex(final int index) {
        if (index < 0) {
            return Optional.empty();
        }
        return Optional.ofNullable(projectRepository.findByIndex(index));
    }

    /**
     * Поиск проекта по наименованию
     *
     * @param name наименование проекта
     * @return проект {@link Project}
     */
    private Optional<Project> findByName(final String name) {
        if (name == null || name.isEmpty()) {
            return Optional.empty();
        }
        return Optional.ofNullable(projectRepository.findByName(name));
    }

    /**
     * Поиск проектов по ID пользователя
     *
     * @param userId код пользователя
     * @return список пользовательских проектов {@link List}
     * @see Project
     */
    private List<Project> findByUserId(UUID userId) {
        return projectRepository.findByUserId(userId);
    }

    /**
     * Поиск проекта по коду
     *
     * @param id код проекта
     * @return проект {@link Project}
     */
    private Optional<Project> findById(final Long id) {
        if (id == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(projectRepository.findById(id));
    }

    /**
     * Удаление проекта по коду
     *
     * @param id код проекта
     * @return проект, который был удален {@link Project}
     */
    public Optional<Project> removeById(final Long id) {
        if (id == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(projectRepository.removeById(id));
    }

    /**
     * Удален епроекта по индексу
     *
     * @param index индекс проекта
     * @return проект, который был удален {@link Project}
     */
    private Optional<Project> removeByIndex(final int index) {
        if (index < 0 || index > projectRepository.size() - 1) {
            return Optional.empty();
        }
        return Optional.ofNullable(projectRepository.removeByIndex(index));
    }

    /**
     * Удаление проекта по названию.
     *
     * @param name название проекта
     * @return проект, который был удален {@link Project}
     */
    private Optional<Project> removeByName(final String name) {
        if (name == null || name.isEmpty()) {
            return Optional.empty();
        }
        return Optional.ofNullable(projectRepository.removeByName(name));
    }

    /**
     * Изменение проекта
     *
     * @param id          код проекта
     * @param name        наименование проекта
     * @param description описание проекта
     * @return проект, который изменен {@link Project}
     */
    private Optional<Project> update(final Long id, final String name, final String description) {
        if (id == null || name == null || name.isEmpty() || description == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(projectRepository.update(id, name, description));
    }

    /**
     * Изменение владельца проекта
     *
     * @param projectId код проекта
     * @param userId    код владельца
     * @return проект {@link Project}
     */
    private Optional<Project> updateProjectOwner(Long projectId, UUID userId) {
        if (projectId == null || userId == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(projectRepository.updateProjectOwner(projectId, userId));
    }


    /**
     * Удаление проекта по имени
     *
     * @param params параметры выполнения
     */
    public void removeProjectByName(Map<String, String> params) throws AccessDeniedException, ProjectNotFoundException {

        final String name = params.get(PROJECT_NAME);
        if (!TaskManagerUtil.checkEmptyInput(name)) {
            throw new IllegalArgumentException("Название проекта не может быть пустым.");
        }
        var project = findByName(name).orElseThrow(() -> new ProjectNotFoundException(PROJECT_NOT_FOUND));
        if (!TaskManagerUtil.checkProjectPrivs(project)) {
            throw new AccessDeniedException(PROJECT_NOT_ACCESIBLE);
        }
        removeByName(name).orElseThrow(() -> new ProjectNotFoundException(PROJECT_NOT_FOUND));
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление проекта по коду
     */
    public void removeProjectById(Map<String, String> params) throws AccessDeniedException, ProjectNotFoundException {
        final Long projectId;
        try {
            projectId = Long.parseLong(params.get(PROJECT_ID));
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException("Код проекта не верен!");
        }
        var project = findById(projectId).orElseThrow(() -> new ProjectNotFoundException(PROJECT_NOT_FOUND));
        if (!TaskManagerUtil.checkProjectPrivs(project)) {
            throw new AccessDeniedException(PROJECT_NOT_ACCESIBLE);
        }
        removeById(projectId).orElseThrow(() -> new ProjectNotFoundException(PROJECT_NOT_FOUND));
    }


    /**
     * Удаление проекта по индексу
     */
    public void removeProjectByIndex(Map<String, String> params) throws ProjectNotFoundException, AccessDeniedException {
        final int projectIndex;
        try {
            projectIndex = Integer.parseInt(params.get(PROJECT_INDEX));

        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(PROJECT_INDEX_FAIL);
        }
        var project = findByIndex(projectIndex).orElseThrow(() -> new ProjectNotFoundException(PROJECT_NOT_FOUND));
        if (!TaskManagerUtil.checkProjectPrivs(project)) {
            throw new AccessDeniedException(PROJECT_NOT_ACCESIBLE);
        }
        removeByIndex(projectIndex).orElseThrow(() -> new ProjectNotFoundException(PROJECT_NOT_FOUND));
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Изменение проекта по коду
     */
    public void updateProjectById(Map<String, String> params) throws ProjectNotFoundException {
        final long projectId;
        try {
            projectId = Long.parseLong(params.get(PROJECT_ID));
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException("Неверный код проекта!");
        }
        final var projectName = params.get(PROJECT_NAME);
        final var projectDescription = params.get("projectDescription");
        update(projectId, projectName, projectDescription).orElseThrow(() -> new ProjectNotFoundException(PROJECT_UPDATE_FAIL));
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Изменение проекта по индексу
     */
    public void updateProjectByIndex(Map<String, String> params) throws ProjectNotFoundException {
        final int projectIndex;
        try {
            projectIndex = Integer.parseInt(params.get(PROJECT_INDEX)) - 1;
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(PROJECT_INDEX_FAIL);
        }
        final var project = findByIndex(projectIndex).orElseThrow(() -> new ProjectNotFoundException(PROJECT_NOT_FOUND));
        final var projectName = params.get(PROJECT_NAME);
        final var projectDescription = params.get("projectDescription");
        update(project.getId(), projectName, projectDescription).orElseThrow(() -> new ProjectNotFoundException(PROJECT_UPDATE_FAIL));
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Просмотр проекта по индексу
     */
    public void viewProjectByIndex(Map<String, String> params) {
        final int projectIndex;
        try {
            projectIndex = Integer.parseInt(params.get(PROJECT_INDEX)) - 1;
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(PROJECT_INDEX_FAIL);
        }
        findByIndex(projectIndex).ifPresent(this::viewProject);
    }

    /**
     * Просмотр проекта в консоли
     *
     * @param project проект {@link Project}
     */
    private void viewProject(final Project project) {
        System.out.println("[View Project]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.print("OWNER: ");
        if (project.getOwnedUser() != null) {
            userService.viewUserById(project.getOwnedUser().toString());
        } else {
            System.out.println("не назначен.");
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Поиск проекта по коду
     *
     * @param projectId код проекта
     * @return проект {@link Project}
     */
    public Project findProjectById(final Long projectId) throws ProjectNotFoundException, AccessDeniedException {
        if (projectId == null) {
            return null;
        }
        var project = findById(projectId).orElseThrow(() -> new ProjectNotFoundException(PROJECT_NOT_FOUND));
        if (!TaskManagerUtil.checkProjectPrivs(project)) {
            throw new AccessDeniedException(PROJECT_NOT_ACCESIBLE);
        }
        return project;
    }

    /**
     * Создание проекта
     *
     * @param params параметры выполнения
     */
    public Project createProject(Map<String, String> params) {
        final String projectName = params.get(PROJECT_NAME);
        if (!TaskManagerUtil.checkEmptyInput(projectName)) {
            throw new IllegalArgumentException("Имя проекта не может быть пустым!");
        }
        return create(projectName, userService.getAppUser().getUserId()).orElseThrow(()
                -> new IllegalArgumentException(PROJECT_CREATE_FAIL));
    }

    /**
     * Создание проекта
     *
     * @param projectName имя проекта
     */
    private Project createProject(final String projectName) {
        if (!TaskManagerUtil.checkEmptyInput(projectName)) {
            throw new IllegalArgumentException("Имя проекта не может быть пустым!");
        }
        return create(projectName, null).orElseThrow(()
                -> new IllegalArgumentException(PROJECT_CREATE_FAIL));
    }

    /**
     * Очистка проекта
     */
    public void clearProject() {
        if (!userService.getAppUser().isAdmin()) {
            List<Project> projectList = findByUserId(userService.getAppUser().getUserId());
            if (projectList.isEmpty()) {
                throw new IllegalArgumentException("У данного пользователя нет ни одного проекта.");
            }
            clearListProject(projectList);
        } else {
            clear();
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Список проектов
     */
    public void listProject() {
        List<Project> projectList = findAll();
        if (!userService.getAppUser().isAdmin()) {
            projectList = findAll().stream().filter(TaskManagerUtil::checkProjectPrivs).collect(Collectors.toList());
        }
        System.out.println(projectList);
        TaskManagerUtil.printAndReturnOk();
    }

    private Project checkParams(final Long projectId, final Long taskId) throws AccessDeniedException, ProjectNotFoundException {
        if (projectId == null) {
            throw new IllegalArgumentException("Код проекта не определен.");
        }
        if (taskId == null) {
            throw new IllegalArgumentException("Код задачи не определен.");
        }
        var project = findById(projectId).orElseThrow(()
                -> new ProjectNotFoundException(PROJECT_NOT_FOUND));
        if (project != null && !TaskManagerUtil.checkProjectPrivs(project)) {
            throw new AccessDeniedException(PROJECT_NOT_ACCESIBLE);
        }
        return project;
    }

    /**
     * Добавление задачи к проекту
     *
     * @param projectId код проекта
     * @param taskId    код задачи
     */
    public void addTaskToProject(final Long projectId, final Long taskId)
            throws AccessDeniedException, ProjectNotFoundException {
        var project = checkParams(projectId, taskId);
        if (project == null) {
            throw new ProjectNotFoundException(PROJECT_NOT_FOUND);
        }
        project.getTasks().add(taskId);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи из проекта
     *
     * @param projectId код проекта
     * @param taskId    код задачи
     */
    public void removeTaskFromProject(final Long projectId, final Long taskId)
            throws AccessDeniedException, ProjectNotFoundException {
        var project = checkParams(projectId, taskId);
        if (project == null) {
            throw new ProjectNotFoundException(PROJECT_NOT_FOUND);
        }
        project.getTasks().remove(taskId);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Назначение пользователя на проект
     *
     * @param params параметры выполнения
     */
    public void assignProjectToUser(Map<String, String> params) throws AccessDeniedException, ProjectNotFoundException {
        if (!userService.getAppUser().isAdmin()) {
            throw new AccessDeniedException("Вы не имеете прав на смену владельца для проекта!");
        }
        long projectId;
        try {
            projectId = Long.parseLong(params.get(PROJECT_ID));
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException("Код проекта не определен!");
        }
        UUID uuid;
        try {
            uuid = UUID.fromString(params.get("userId"));
        } catch (IllegalArgumentException ie) {
            throw new IllegalArgumentException("Код пользователя не верен!".concat("\n").concat(ie.getMessage()));
        }
        updateProjectOwner(projectId, uuid).orElseThrow(()
                -> new ProjectNotFoundException(PROJECT_UPDATE_FAIL));
        TaskManagerUtil.printAndReturnOk();
    }

}
