package com.nlmk.evteev.tm.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class UIService {

    private static final Logger logger = LogManager.getLogger(UIService.class);
    private static UIService instance = null;
    private final Scanner scanner = new Scanner(System.in);

    public static UIService getInstance() {
        return instance == null
                ? instance = new UIService()
                : instance;
    }

    public Map<String, String> doUserCreate() {
        Map<String, String> paramsMap = new HashMap<>();
        logger.info("[CREATE USER]");
        System.out.println("Введите регистрационное имя:");
        final String loginName = scanner.nextLine();
        paramsMap.put("loginName", loginName);
        doEnterUserPassword(paramsMap);
        System.out.println("Предоставить административные права? (y - да, n - нет):");
        final String str = scanner.nextLine();
        paramsMap.put("userRoleAdmin", str);
        return paramsMap;
    }

    public void doEnterUserPassword(Map<String, String> paramsMap) {
        System.out.println("Введите пароль:");
        final String password = scanner.nextLine();
        paramsMap.put("password", password);
    }

    public Map<String, String> doUserUpdate(boolean withAdmin) {
        Map<String, String> paramsMap = new HashMap<>();
        logger.info("[UPDATE USER DATA]");
        doEnterUserLogin(paramsMap);
        System.out.println("Введите фамилию пользователя:");
        String firstName = scanner.nextLine();
        paramsMap.put("firstName", firstName);
        System.out.println("Введите имя пользователя:");
        String middleName = scanner.nextLine();
        paramsMap.put("middleName", middleName);
        System.out.println("Введите отчество пользователя:");
        String secondName = scanner.nextLine();
        paramsMap.put("secondName", secondName);
        doEnterUserNewPassword(paramsMap);
        if (withAdmin) {
            setAdminRole(paramsMap);
        }
        return paramsMap;
    }

    public void doEnterUserLogin(Map<String, String> params) {
        System.out.println("Введите регистрационное имя пользователя:");
        final String loginName = scanner.nextLine();
        params.put("loginName", loginName);
    }

    public void doEnterUserCode(Map<String, String> params) {
        System.out.println("Введите код пользователя");
        final String userId = scanner.nextLine();
        params.put("userId", userId);
    }

    private void setAdminRole(Map<String, String> params) {
        System.out.println("Предоставить административные права? (y - да, n - нет):");
        final String str = scanner.nextLine();
        params.put("userRoleAdmin", str);
    }


    public void doEnterUserNewPassword(Map<String, String> params) {
        System.out.println("Введите новый пароль пользователя:");
        String password = scanner.nextLine();
        params.put("password", password);
    }

    public void doEnterTaskName(Map<String, String> params) {
        System.out.println("Введите имя задачи: ");
        final String taskName = scanner.nextLine();
        params.put("taskName", taskName);
    }

    public void doEnterTaskId(Map<String, String> params) {
        System.out.println("Введите ID задачи: ");
        final String taskId = scanner.nextLine();
        params.put("taskId", taskId);
    }

    public void doEnterTaskIndex(Map<String, String> params) {
        System.out.println("Введите индекс задачи: ");
        final String taskIndex = scanner.nextLine();
        params.put("taskIndex", taskIndex);
    }

    public void doEnterNewTaskNameAndDescription(Map<String, String> params) {
        System.out.println("Введите новое название задачи: ");
        final String taskName = scanner.nextLine();
        params.put("taskName", taskName);
        System.out.println("Введите новое описание задачи: ");
        final String taskDescription = scanner.nextLine();
        params.put("taskDescription", taskDescription);
    }

    public void doEnterTaskDeadLine(Map<String, String> params) {
        System.out.println("Укажите время окончания задачи (мин):");
        final String deadLine = scanner.nextLine();
        params.put("deadLine", deadLine);
    }

    public void doEnterProjectId(Map<String, String> params) {
        System.out.println("Введите код проекта: ");
        final String projectId = scanner.nextLine();
        params.put("projectId", projectId);
    }

    public void doEnterProjectIndex(Map<String, String> params) {
        System.out.println("Введите индекс проекта: ");
        final String projectIndex = scanner.nextLine();
        params.put("projectIndex", projectIndex);
    }

    public void doEnterProjectName(Map<String, String> params) {
        System.out.println("Введите имя проекта: ");
        final String projectName = scanner.nextLine();
        params.put("projectName", projectName);
    }

    public void doEnterProjectDescription(Map<String, String> params) {
        System.out.println("Введите описание проекта: ");
        final String projectDescription = scanner.nextLine();
        params.put("projectDescription", projectDescription);
    }

}
