package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.exceptions.TaskNotFoundException;
import com.nlmk.evteev.tm.repository.TaskRepository;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;

/**
 * Сервисный класс для работы с репозиторием задач
 */
public class TaskService {

    private final TaskRepository taskRepository;

    private final Logger logger = LogManager.getLogger(TaskService.class);

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * Создание задачи и добавление ее в список задач
     *
     * @param name имя задачи
     * @return объект типа {@link Task}
     */
    private Optional<Task> create(final String name, final Integer deadLine) {
        if (name == null || name.isEmpty()) {
            return Optional.empty();
        }
        var task = taskRepository.create(name);
        task.setDeadLine(LocalDateTime.now().plus(deadLine, ChronoUnit.MINUTES));
        return Optional.ofNullable(task);
    }

    /**
     * Очистка списка задач
     */
    private void clear() {
        taskRepository.clear();
    }

    /**
     * Возвращает список задач
     *
     * @return список задач {@link List}
     */
    private List<Task> findAll() {
        return taskRepository.findAll();
    }

    /**
     * Поиск задачи по индексу
     *
     * @param index индекс задачи
     * @return задача {@link Task}
     */
    private Optional<Task> findByIndex(final int index) {
        if (index < 0) {
            return Optional.empty();
        }
        return Optional.ofNullable(taskRepository.findByIndex(index));
    }

    /**
     * Поиск задачи по наименованию
     *
     * @param name наименование задачи
     * @return задача {@link Task}
     */
    private Optional<Task> findByName(final String name) {
        if (name == null || name.isEmpty()) {
            return Optional.empty();
        }
        return Optional.ofNullable(taskRepository.findByName(name));
    }

    /**
     * Поиск задачи по коду
     *
     * @param id колд задачи
     * @return задача {@link Task}
     */
    private Optional<Task> findById(final Long id) {
        if (id == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(taskRepository.findById(id));
    }

    /**
     * Удаление задачи по коду
     *
     * @param id код задачи
     * @return удаленная задача {@link Task}
     */
    public Optional<Task> removeById(final Long id) {
        if (id == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(taskRepository.removeById(id));
    }

    /**
     * Удаление задачи по наименованию
     *
     * @param name наименование задачи
     * @return удаленная задача {@link Task}
     */
    private Optional<Task> removeByName(final String name) {
        if (name == null || name.isEmpty()) {
            return Optional.empty();
        }
        return Optional.ofNullable(taskRepository.removeByName(name));
    }

    /**
     * Удаление задачи по индексу
     *
     * @param index индекс задачи
     * @return удаленная задача {@link Task}
     */
    private Optional<Task> removeByIndex(final Integer index) {
        if (index < 0 || index > taskRepository.size() - 1) {
            return Optional.empty();
        }
        return Optional.ofNullable(taskRepository.removeByIndex(index));
    }

    /**
     * Удаление задачи по названию
     *
     * @param params параметры выполнения
     */
    public void removeTaskByName(Map<String, String> params) throws TaskNotFoundException {
        final String taskName = params.get(TASK_NAME);
        removeByName(taskName).orElseThrow(()
                -> new TaskNotFoundException(String.format("Задача c названием %s не найдена!", taskName)));
    }

    /**
     * Удаление задачи по коду
     *
     * @param params параметры выполнения
     */
    public void removeTaskById(Map<String, String> params) throws TaskNotFoundException {
        long taskId;
        try {
            taskId = Long.parseLong(params.get("taskId"));
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(ne.getMessage());
        }
        removeById(taskId).orElseThrow(() -> new TaskNotFoundException(TASK_DELETE_FAIL));
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи по индексу
     *
     * @param params параметры выполнения
     */
    public void removeTaskByIndex(Map<String, String> params) throws TaskNotFoundException {
        int taskIndex;
        try {
            taskIndex = Integer.parseInt(params.get(TASK_INDEX));
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(ne.getMessage());
        }
        removeByIndex(taskIndex).orElseThrow(() -> new TaskNotFoundException(TASK_DELETE_FAIL));
    }


    /**
     * Изменение задачи
     *
     * @param id          код задачи
     * @param name        наименование задачи
     * @param description описание задачи
     * @return измененная задача {@link Task}
     */
    private Optional<Task> update(final Long id, final String name, final String description) {
        if (id == null || name == null || name.isEmpty() || description == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(taskRepository.update(id, name, description));
    }

    /**
     * Изменение задачи по индексу
     *
     * @param params параметры выполнения
     */
    public void updateTaskByIndex(Map<String, String> params) {
        final int taskIndex;
        try {
            taskIndex = Integer.parseInt(params.get(TASK_INDEX)) - 1;
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(ne.getMessage());
        }
        final var taskName = params.get(TASK_NAME);
        final var taskDescription = params.get(TASK_DESCR);
        findByIndex(taskIndex).ifPresentOrElse(task ->
                        update(task.getId(), taskName, taskDescription),
                () -> new TaskNotFoundException("задача не найдена"));
    }

    /**
     * Изменение задачи по коду
     *
     * @param params параметры выполнения
     */
    public void updateTaskById(Map<String, String> params) throws TaskNotFoundException {
        final long taskId;
        try {
            taskId = Long.parseLong(params.get("taskId"));
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(ne.getMessage());
        }
        final var taskName = params.get(TASK_NAME);
        final var taskDescription = params.get(TASK_DESCR);
        update(taskId, taskName, taskDescription).orElseThrow(() -> new TaskNotFoundException(TASK_UPDATE_FAIL));
    }

    /**
     * Список задач, привязанных к проекту
     *
     * @param projectId код проекта
     * @return список типа {@link List}
     * @see Task
     */
    private List<Task> findAllByProjectId(final Long projectId) {
        if (projectId == null) {
            return Collections.emptyList();
        }
        return taskRepository.findAllByProjectId(projectId);
    }


    /**
     * Удаление задачи из проекта
     *
     * @param taskId код задачи
     * @return задача, удаленная из проекта
     * @see Task
     */
    private Optional<Task> removeTask(final Long taskId) {
        if (taskId == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(taskRepository.removeTaskFromProject(taskId));
    }

    /**
     * Просмотр задачи в консоли
     *
     * @param task задача {@link Task}
     */
    public void viewTask(final Task task) {
        if (task == null) {
            return;
        }
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("PROJECT_ID: " + task.getProjectId());
    }

    /**
     * Просмотр задачи по индексу
     *
     * @param params параметры выполнения
     */
    public void viewTaskByIndex(Map<String, String> params) {
        final int taskIndex;
        try {
            taskIndex = Integer.parseInt(params.get(TASK_INDEX)) - 1;
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(ne.getMessage());
        }
        findByIndex(taskIndex).ifPresentOrElse(task1 -> viewTask(task1),
                () -> new TaskNotFoundException(String.format("Задача с индексом %d не найдена!", taskIndex)));
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Создание задачи
     *
     * @param params параметры выполнения
     */
    public void createTask(Map<String, String> params) {
        final String taskName = params.get(TASK_NAME);
        int deadLine;
        try {
            deadLine = Integer.parseInt(params.get("deadLine"));
        } catch (NullPointerException | NumberFormatException ne) {
            throw new IllegalArgumentException("Параметры не верны");
        }
        create(taskName, deadLine).orElseThrow(() -> new IllegalArgumentException("Задача не создана!"));
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Очистка задачи
     */
    public void clearTask() {
        clear();
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Список задач
     */
    public void listTask() {
        findAll().forEach(this::viewTask);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Поиск задачи по ID
     *
     * @param taskId код задачи
     * @return задача {@link Task}
     */
    public Optional<Task> findTaskById(final Long taskId) {
        if (taskId == null) {
            return Optional.empty();
        }
        return findById(taskId);
    }

    /**
     * Список задач, привязанных к проекту
     *
     * @param params параметры выполнения
     */
    public void findAllByProjectId(Map<String, String> params) throws TaskNotFoundException {
        final long projectId;
        try {
            projectId = Long.parseLong(params.get("projectId"));
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(ne.getMessage());
        }
        var tasks = findAllByProjectId(projectId);
        if (tasks.isEmpty()) {
            throw new TaskNotFoundException("Задач для проекта не найдено");
        } else {
            tasks.forEach(this::viewTask);
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Список задач, непривязанных к проектам
     */
    public void findAllWithoutProject() throws TaskNotFoundException {
        var tasks = taskRepository.findAllWithoutProject();
        if (tasks.isEmpty()) {
            throw new TaskNotFoundException("Задач, непривязанных к проектам не найдено");
        } else {
            tasks.forEach(this::viewTask);
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи из проекта
     *
     * @param taskId код задачи
     */
    public void removeTaskFromProject(final Long taskId) throws TaskNotFoundException {
        if (taskId == null) {
            return;
        }
        removeTask(taskId).orElseThrow(() -> new TaskNotFoundException("Задача не найдена!"));
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Уведомление об истечении срока задачи
     */
    public void notifyTaskDeadLine() {
        CompletableFuture.runAsync(() -> {
            long betweenDate;
            while (TaskManagerUtil.hasNotifyDeadLineFlag) {
                for (var task : taskRepository.findAll()) {
                    betweenDate = ChronoUnit.SECONDS.between(LocalDateTime.now(), task.getDeadLine());
                    if ((task.getDeadLine() == null) && (betweenDate < 0)) {
                        continue;
                    }
                    doNotify();
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException exception) {
                        logger.error(exception.getMessage());
                        Thread.currentThread().interrupt();
                    }
                }
            }
        });
    }

    /**
     * Оповещение о приближении окончания задачи
     */
    private void doNotify() {
        var notifyTime = new ArrayList<>(Arrays.asList(5, 15, 30, 60, 240));
        var notifyTasks = taskRepository.findAll();
        for (var time : notifyTime) {
            notifyTasks.stream().filter(task ->
                    task.getDeadLine().plus(time, ChronoUnit.MINUTES).compareTo(LocalDateTime.now()) <= 0
            ).collect(Collectors.toList()).forEach(task -> {
                if (TaskManagerUtil.checkTaskPrivs(task)) {
                    logger.warn(String.format("~~~~~~Задача %s истекает через %d минут(ы)",
                            task.getName(), time));
                }
            });
        }
    }

    /**
     * Удаление задач с истекшим сроком.
     */
    public void removeOldTask() {
        CompletableFuture.runAsync(() -> {
            while (TaskManagerUtil.hasRemoveOldestTaskFlag) {
                for (var task : taskRepository.findAll().stream().filter(TaskManagerUtil::checkTaskPrivs).collect(Collectors.toList())) {
                    if (LocalDateTime.now().isAfter(task.getDeadLine())) {
                        taskRepository.removeById(task.getId());
                        logger.info(String.format("~~~~~~Задача: %s, код: %d с истекшим сроком. Будет удалена.",
                                task.getName(), task.getId()));
                    }
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException ex) {
                    logger.error(ex.getMessage());
                    Thread.currentThread().interrupt();
                }
            }
        });
    }

}
