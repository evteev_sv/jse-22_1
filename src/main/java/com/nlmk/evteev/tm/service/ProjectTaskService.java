package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.enumerations.ExportType;
import com.nlmk.evteev.tm.exceptions.ProjectNotFoundException;
import com.nlmk.evteev.tm.exceptions.TaskNotFoundException;
import com.nlmk.evteev.tm.export.DataTransmitter;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.repository.TaskRepository;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.util.Scanner;

import static com.nlmk.evteev.tm.constant.TerminalConst.PROJECT_CODE_FAIL;
import static com.nlmk.evteev.tm.constant.TerminalConst.PROJECT_NOT_FOUND;

public class ProjectTaskService {

    private static final Logger logger = LogManager.getLogger(ProjectTaskService.class);
    private static ProjectTaskService instance = null;
    protected final Scanner scanner = new Scanner(System.in);
    private final TaskRepository taskRepository = new TaskRepository();
    private final TaskService taskService = new TaskService(taskRepository);
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final ProjectService projectService = new ProjectService(projectRepository);

    private ProjectTaskService() {
    }

    public static ProjectTaskService getInstance() {
        synchronized (ProjectTaskService.class) {
            return instance == null
                    ? instance = new ProjectTaskService()
                    : instance;
        }
    }

    /**
     * Просмотр проекта вместе с задачами
     *
     * @throws ProjectNotFoundException выкидывет ошибку поиска проекта {@link ProjectNotFoundException}
     */
    public void viewProjectWithTasks() throws ProjectNotFoundException, AccessDeniedException {
        System.out.println("Введите код проекта:");
        final Long projectId;
        try {
            projectId = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException ne) {
            logger.error("Id проекта не определено!");
            return;
        }
        var project = projectService.findProjectById(projectId);
        if (project == null) {
            throw new ProjectNotFoundException(PROJECT_NOT_FOUND);
        }
        System.out.println(project);
        if (!project.getTasks().isEmpty()) {
            System.out.println("Задачи, назначенные на проект: ");
            project.getTasks().forEach(aLong ->
                    taskService.findTaskById(aLong).ifPresent(task1 -> taskService.viewTask(task1)));
            TaskManagerUtil.printAndReturnOk();
        } else {
            TaskManagerUtil.printAndReturnFail("К данному проекту не привязана ни одна задача.");
        }
    }


    /**
     * Добавление задачи к проекту по id
     *
     * @throws ProjectNotFoundException ошибки при поиске проекта {@link ProjectNotFoundException}
     * @throws TaskNotFoundException    ошибки при поиске задачи {@link TaskNotFoundException}
     */
    public void addTaskToProjectByIds() throws ProjectNotFoundException, TaskNotFoundException, AccessDeniedException {
        System.out.println("Введите код проекта:");
        long projectId;
        try {
            projectId = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(PROJECT_NOT_FOUND);
        }
        var project = projectService.findProjectById(projectId);
        if (project == null) {
            throw new ProjectNotFoundException(PROJECT_NOT_FOUND);
        }
        System.out.println("Введите код задачи:");
        long taskId;
        try {
            taskId = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException("Код задачи не определен!");
        }
        var task = taskService.findTaskById(taskId).orElseThrow(() -> new TaskNotFoundException("Задача не найдена!"));
        taskRepository.addTaskToProjectByIds(task.getProjectId(), task.getId());
        projectService.addTaskToProject(projectId, taskId);
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи из проекта по коду
     *
     * @throws ProjectNotFoundException ошибки при поиске проекта {@link ProjectNotFoundException}
     * @throws TaskNotFoundException    ошибки при поиске задачи {@link TaskNotFoundException}
     */
    public void removeTaskFromProjectById() throws ProjectNotFoundException, TaskNotFoundException, AccessDeniedException {
        System.out.println("Введите код проекта:");
        long projectId;
        try {
            projectId = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(PROJECT_CODE_FAIL);
        }
        System.out.println("Введите код задачи:");
        long taskId;
        try {
            taskId = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException("Код задачи не определен!");
        }
        var project = projectService.findProjectById(projectId);
        if (project == null) {
            throw new ProjectNotFoundException("Проект не найден!");
        }
        var task = taskService.findTaskById(taskId).orElseThrow(() -> new TaskNotFoundException("Задача не найдена!"));
        taskService.removeTaskFromProject(task.getId());
        projectService.removeTaskFromProject(task.getProjectId(), task.getId());
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Очистка задач проекта
     */
    public void clearProjectTasks() throws ProjectNotFoundException, AccessDeniedException {
        System.out.println("Введите код проекта:");
        long projectId;
        try {
            projectId = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(PROJECT_CODE_FAIL);
        }
        var project = projectService.findProjectById(projectId);
        if (!project.getTasks().isEmpty()) {
            project.getTasks().forEach(aLong ->
                    taskService.findTaskById(projectId).ifPresent(task1 -> {
                        try {
                            taskService.removeTaskFromProject(task1.getId());
                        } catch (TaskNotFoundException e) {
                            e.printStackTrace();
                        }
                    }));
            project.getTasks().clear();
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление проекта вместе с задачами
     */
    public void removeProjectWithTasks() throws ProjectNotFoundException, AccessDeniedException {
        System.out.println("Введите код проекта:");
        long projectId;
        try {
            projectId = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException ne) {
            throw new IllegalArgumentException(PROJECT_CODE_FAIL);
        }
        var project = projectService.findProjectById(projectId);
        if (!project.getTasks().isEmpty()) {
            project.getTasks().forEach(aLong ->
                    taskService.findTaskById(project.getId()).ifPresent(task1 -> taskService.removeById(task1.getId())));
            projectService.removeById(projectId);
        }
        TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Экспортирование данных в файл
     *
     * @param exportType тип экспорта {@link ExportType}
     */
    public void exportData(ExportType exportType) {
        var projectDataExporter = new DataTransmitter<>(exportType, projectRepository);
        var taskDataExporter = new DataTransmitter<>(exportType, taskRepository);
        var ext = exportType == ExportType.TYPE_JSON ? ".json" : ".xml";
        try {
            projectDataExporter.exportToFile("proj_export".concat(ext));
            taskDataExporter.exportToFile("task_export".concat(ext));
            logger.info("[OK] Экспорт завершен.");
        } catch (IOException e) {
            logger.error("Ошибка экспорта данных в файл! ".concat(e.getMessage()));
        }
    }

    public void importData(ExportType exportType) {
        var projectDataExporter = new DataTransmitter<>(exportType, projectRepository);
        var taskDataExporter = new DataTransmitter<>(exportType, taskRepository);
        var ext = exportType == ExportType.TYPE_JSON ? ".json" : ".xml";
        try {
            projectDataExporter.importFromFile("proj_export".concat(ext));
            taskDataExporter.importFromFile("task_export".concat(ext));
            logger.info("[OK] импорт завершен.");
        } catch (IOException e) {
            logger.error("Ошибка импорта данных из файла! ".concat(e.getMessage()));
        }

    }
}
