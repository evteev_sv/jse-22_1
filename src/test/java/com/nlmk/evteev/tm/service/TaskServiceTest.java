package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.constant.TerminalConst;
import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.exceptions.TaskNotFoundException;
import com.nlmk.evteev.tm.repository.TaskRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.util.*;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class TaskServiceTest {

    private final TaskRepository mockTaskRepository = mock(TaskRepository.class);
    private final Task expected = new Task("TEST");
    private TaskService taskService;

    @BeforeEach
    void setUp() {
        taskService = new TaskService(mockTaskRepository);
    }

    @Test
    void removeById() {
        Long id = 12L;
        when(mockTaskRepository.removeById(id)).thenReturn(expected);
        Optional<Task> res = taskService.removeById(id);
        Task result = new Task("");
        if (res.isPresent()) {
            result = res.get();
        }
        assertTrue(new ReflectionEquals(expected).matches(result));
    }

    @Test
    void removeByIdEmptyArgs() {
        Optional<Task> result = taskService.removeById(null);
        result.ifPresent(task -> assertNull(result));
    }

    @Test
    void removeTaskByNameException() {
        Map<String, String> params = new HashMap<>();
        params.put(TerminalConst.TASK_NAME, "TEST");
        when(mockTaskRepository.removeByName(any())).thenReturn(null);
        assertThrows(TaskNotFoundException.class, () -> taskService.removeTaskByName(params));
    }

    @Test
    void removeTaskByNameGood() throws TaskNotFoundException {
        Map<String, String> params = new HashMap<>();
        params.put(TerminalConst.TASK_NAME, "TEST");
        when(mockTaskRepository.removeByName(any())).thenReturn(expected);
        taskService.removeTaskByName(params);
        verify(mockTaskRepository, times(1)).removeByName("TEST");
    }

    @Test
    void removeTaskByIdEmptyArgs() {
        Map<String, String> params = new HashMap<>();
        params.put("taskId", "AA");
        assertThrows(IllegalArgumentException.class, () -> taskService.removeTaskById(params));
    }

    @Test
    void removeTaskByIdTaskNotFound() {
        Map<String, String> params = new HashMap<>();
        params.put("taskId", "1");
        when(mockTaskRepository.removeById(1)).thenReturn(null);
        assertThrows(TaskNotFoundException.class, () -> taskService.removeTaskById(params));
    }

    @Test
    void removeTaskByIdGood() throws TaskNotFoundException {
        Map<String, String> params = new HashMap<>();
        params.put("taskId", "1");
        when(mockTaskRepository.removeById(any())).thenReturn(expected);
        taskService.removeTaskById(params);
        verify(mockTaskRepository, times(1)).removeById(any());
    }

    @Test
    void removeTaskByIndexNumberException() {
        Map<String, String> params = new HashMap<>();
        params.put(TASK_INDEX, "AA");
        assertThrows(IllegalArgumentException.class, () -> taskService.removeTaskByIndex(params));
    }

    @Test
    void removeTaskByIndexNotFound() {
        Map<String, String> params = new HashMap<>();
        params.put(TASK_INDEX, "1");
        when(mockTaskRepository.removeByIndex(1)).thenReturn(null);
        assertThrows(TaskNotFoundException.class, () -> taskService.removeTaskByIndex(params));
    }

    @Test
    void removeTaskByIndexGood() throws TaskNotFoundException {
        int index = 1;
        Map<String, String> params = new HashMap<>();
        params.put(TASK_INDEX, "1");
        when(mockTaskRepository.size()).thenReturn(2);
        when(mockTaskRepository.removeByIndex(index)).thenReturn(expected);
        taskService.removeTaskByIndex(params);
        verify(mockTaskRepository, times(1)).removeByIndex(index);
    }

    @Test
    void updateTaskByIndexEmptyIndex() {
        Map<String, String> params = new HashMap<>();
        params.put(TASK_INDEX, "");
        params.put(TASK_NAME, "TEST");
        params.put(TASK_DESCR, "DESCR_TEST");
        assertThrows(IllegalArgumentException.class, () -> taskService.updateTaskByIndex(params));
    }

    @Test
    void updateTaskByIndexCharIndex() {
        Map<String, String> params = new HashMap<>();
        params.put(TASK_INDEX, "AA");
        params.put(TASK_NAME, "TEST");
        params.put(TASK_DESCR, "DESCR_TEST");
        assertThrows(IllegalArgumentException.class, () -> taskService.updateTaskByIndex(params));
    }

    @Test
    void updateTaskByIndexGood() throws TaskNotFoundException {
        Map<String, String> params = new HashMap<>();
        params.put(TASK_INDEX, "1");
        params.put(TASK_NAME, "TEST");
        params.put(TASK_DESCR, "DESCR_TEST");
        when(mockTaskRepository.findByIndex(0)).thenReturn(expected);
        when(mockTaskRepository.update(any(), any(), any())).thenReturn(expected);
        taskService.updateTaskByIndex(params);
        verify(mockTaskRepository, times(1)).update(any(), any(), any());
    }

    @Test
    void updateTaskByIdEmptyId() {
        Map<String, String> params = new HashMap<>();
        params.put("taskId", "");
        params.put(TASK_NAME, "TEST");
        params.put(TASK_DESCR, "DESCR_TEST");
        assertThrows(IllegalArgumentException.class, () -> taskService.updateTaskById(params));
    }

    @Test
    void updateTaskByIdNotFound() {
        Map<String, String> params = new HashMap<>();
        params.put("taskId", "1");
        params.put(TASK_NAME, "TEST");
        params.put(TASK_DESCR, "DESCR_TEST");
        when(mockTaskRepository.update(any(), any(), any())).thenReturn(null);
        assertThrows(TaskNotFoundException.class, () -> taskService.updateTaskById(params));
    }

    @Test
    void updateTaskByIdGood() throws TaskNotFoundException {
        Map<String, String> params = new HashMap<>();
        params.put("taskId", "1");
        params.put(TASK_NAME, "TEST");
        params.put(TASK_DESCR, "DESCR_TEST");
        when(mockTaskRepository.update(any(), any(), any())).thenReturn(expected);
        taskService.updateTaskById(params);
        verify(mockTaskRepository, times(1)).update(any(), any(), any());
    }

    @Test
    void viewTaskByIndexEmptyParam() {
        Map<String, String> params = new HashMap<>();
        params.put(TASK_INDEX, "");
        params.put(TASK_NAME, "TEST");
        params.put(TASK_DESCR, "DESCR_TEST");
        assertThrows(IllegalArgumentException.class, () -> taskService.viewTaskByIndex((params)));
    }

    @Test
    void viewTaskByIndexGood() throws TaskNotFoundException {
        Map<String, String> params = new HashMap<>();
        params.put(TASK_INDEX, "1");
        params.put(TASK_NAME, "TEST");
        params.put(TASK_DESCR, "DESCR_TEST");
        when(mockTaskRepository.findByIndex(0)).thenReturn(expected);
        taskService.viewTaskByIndex(params);
        verify(mockTaskRepository, times(1)).findByIndex(0);
    }

    @Test
    void createTaskEmptyName() {
        Map<String, String> params = new HashMap<>();
        params.put(TASK_NAME, "");
        params.put(TASK_DESCR, "DESCR_TEST");
        assertThrows(IllegalArgumentException.class, () -> taskService.createTask(params));
    }

    @Test
    void createTaskGood() {
        Map<String, String> params = new HashMap<>();
        params.put(TASK_NAME, "TEST");
        params.put(TASK_DESCR, "DESCR_TEST");
        params.put("deadLine", "1");
        when(mockTaskRepository.create("TEST")).thenReturn(expected);
        taskService.createTask(params);
        verify(mockTaskRepository, times(1)).create("TEST");
    }

    @Test
    void findTaskByIdEmptyParam() {
        when(mockTaskRepository.findById(1L)).thenReturn(expected);
        Optional<Task> result = taskService.findTaskById(null);
        result.ifPresent(task -> assertNull(result));
    }

    @Test
    void findTaskByIdNotFound() {
        when(mockTaskRepository.findById(1L)).thenReturn(null);
        Optional<Task> result = taskService.findTaskById(1L);
        result.ifPresent(task -> assertNull(result));
    }

    @Test
    void findTaskByIdGood() {
        when(mockTaskRepository.findById(1L)).thenReturn(expected);
        Task result = taskService.findTaskById(1L).get();
        assertTrue(new ReflectionEquals(expected).matches(result));
    }

    @Test
    void findAllByProjectId() throws TaskNotFoundException {
        Map<String, String> params = new HashMap<>();
        params.put("projectId", "1");
        params.put(TASK_NAME, "TEST");
        params.put(TASK_DESCR, "DESCR_TEST");
        List<Task> expectedList = new ArrayList<>();
        expectedList.add(expected);
        when(mockTaskRepository.findAllByProjectId(1L)).thenReturn(expectedList);
        taskService.findAllByProjectId(params);
        verify(mockTaskRepository, times(1)).findAllByProjectId(1L);
    }

    @Test
    void findAllByProjectIdEmptyList() {
        Map<String, String> params = new HashMap<>();
        params.put("projectId", "1");
        params.put(TASK_NAME, "TEST");
        params.put(TASK_DESCR, "DESCR_TEST");
        List<Task> expectedList = new ArrayList<>();
        when(mockTaskRepository.findAllByProjectId(1L)).thenReturn(expectedList);
        assertThrows(TaskNotFoundException.class, () -> taskService.findAllByProjectId(params));
    }

    @Test
    void findAllByProjectIdEmptyId() {
        Map<String, String> params = new HashMap<>();
        params.put("projectId", "");
        params.put(TASK_NAME, "TEST");
        params.put(TASK_DESCR, "DESCR_TEST");
        assertThrows(IllegalArgumentException.class, () -> taskService.findAllByProjectId(params));
    }

    @Test
    void findAllWithoutProjectEmptyList() {
        List<Task> expectedList = new ArrayList<>();
        when(mockTaskRepository.findAllWithoutProject()).thenReturn(expectedList);
        assertThrows(TaskNotFoundException.class, () -> taskService.findAllWithoutProject());
    }

    @Test
    void findAllWithoutProject() throws TaskNotFoundException {
        List<Task> expectedList = new ArrayList<>();
        expectedList.add(expected);
        when(mockTaskRepository.findAllWithoutProject()).thenReturn(expectedList);
        taskService.findAllWithoutProject();
        verify(mockTaskRepository, times(1)).findAllWithoutProject();
    }

    @Test
    void removeTaskFromProjectException() {
        when(mockTaskRepository.removeTaskFromProject(1L)).thenReturn(null);
        assertThrows(TaskNotFoundException.class, () -> taskService.removeTaskFromProject(1L));
    }

    @Test
    void removeTaskFromProject() throws TaskNotFoundException {
        when(mockTaskRepository.removeTaskFromProject(1L)).thenReturn(expected);
        taskService.removeTaskFromProject(1L);
        verify(mockTaskRepository, times(1)).removeTaskFromProject(1L);
    }
}