package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.enumerations.UserRole;
import com.nlmk.evteev.tm.exceptions.ProjectNotFoundException;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import com.nlmk.evteev.tm.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.nio.file.AccessDeniedException;
import java.util.*;

import static com.nlmk.evteev.tm.constant.TerminalConst.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ProjectServiceTest {

    private final ProjectRepository mockRepository = mock(ProjectRepository.class);
    private ProjectService projectService;
    private final Project expected = new Project("TEST");

    @BeforeEach
    void setUp() {
        projectService = new ProjectService(mockRepository);
    }

    @Test
    void removeByIdEmptyId() {
        when(mockRepository.removeById(any())).thenReturn(expected);
        Optional<Project> result = projectService.removeById(null);
        assertTrue(result.isEmpty());
    }

    @Test
    void removeByIdGood() {
        when(mockRepository.removeById(1L)).thenReturn(expected);
        Project result = projectService.removeById(1L).get();
        assertTrue(new ReflectionEquals(expected).matches(result));
    }

    @Test
    void removeProjectByNameEmptyName() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "");
        assertThrows(IllegalArgumentException.class, () -> projectService.removeProjectByName(params));
    }

    @Test
    void removeProjectByNameProjectNotFound() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "TEST");
        when(mockRepository.findByName("TEST")).thenReturn(null);
        assertThrows(ProjectNotFoundException.class, () -> projectService.removeProjectByName(params));
    }

    @Test
    void removeProjectByNameGood() throws AccessDeniedException, ProjectNotFoundException {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);

        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "TEST");
        when(mockRepository.findByName("TEST")).thenReturn(expected);
        when(mockRepository.removeByName("TEST")).thenReturn(expected);
        projectService.removeProjectByName(params);
        verify(mockRepository, times(1)).removeByName("TEST");
    }

    @Test
    void removeProjectByNameProjetcNotFound2() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);

        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "TEST");
        when(mockRepository.findByName("TEST")).thenReturn(expected);
        when(mockRepository.removeByName("TEST")).thenReturn(null);
        assertThrows(ProjectNotFoundException.class, () -> projectService.removeProjectByName(params));
    }

    @Test
    void removeProjectByNameAccessibleNot() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.USER);
        userService.setAppUser(user);

        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "TEST");
        when(mockRepository.findByName("TEST")).thenReturn(expected);
        assertThrows(AccessDeniedException.class, () -> projectService.removeProjectByName(params));
    }

    @Test
    void removeProjectByIdEmptyId() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_ID, "");
        assertThrows(IllegalArgumentException.class, () -> projectService.removeProjectById(params));
    }

    @Test
    void removeProjectByIdProjectNotFound() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_ID, "1");
        when(mockRepository.findById(1L)).thenReturn(null);
        assertThrows(ProjectNotFoundException.class, () -> projectService.removeProjectById(params));
    }

    @Test
    void removeProjectByIdGood() throws AccessDeniedException, ProjectNotFoundException {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);

        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_ID, "1");
        when(mockRepository.findById(1L)).thenReturn(expected);
        when(mockRepository.removeById(1L)).thenReturn(expected);
        projectService.removeProjectById(params);
        verify(mockRepository, times(1)).removeById(1L);
    }

    @Test
    void removeProjectByIdProjectNotFound2() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);

        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_ID, "1");
        when(mockRepository.findById(1L)).thenReturn(expected);
        when(mockRepository.removeById(1L)).thenReturn(null);
        assertThrows(ProjectNotFoundException.class, () -> projectService.removeProjectById(params));
    }

    @Test
    void removeProjectByIdAccessibleNot() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.USER);
        userService.setAppUser(user);

        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_ID, "1");
        when(mockRepository.findById(1L)).thenReturn(expected);
        assertThrows(AccessDeniedException.class, () -> projectService.removeProjectById(params));
    }

    @Test
    void removeProjectByIndexEmptyId() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_INDEX, "");
        assertThrows(IllegalArgumentException.class, () -> projectService.removeProjectByIndex(params));
    }

    @Test
    void removeProjectByIndexProjectNotFound() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_INDEX, "1");
        when(mockRepository.findByIndex(1)).thenReturn(null);
        assertThrows(ProjectNotFoundException.class, () -> projectService.removeProjectByIndex(params));
    }

    @Test
    void removeProjectByIndexGood() throws AccessDeniedException, ProjectNotFoundException {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);

        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_INDEX, "1");

        when(mockRepository.findByIndex(1)).thenReturn(expected);
        when(mockRepository.size()).thenReturn(2);
        when(mockRepository.removeByIndex(1)).thenReturn(expected);
        projectService.removeProjectByIndex(params);
        verify(mockRepository, times(1)).removeByIndex(1);
    }

    @Test
    void removeProjectByIndexProjectNotFound2() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);

        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_INDEX, "1");
        when(mockRepository.findByIndex(1)).thenReturn(expected);
        when(mockRepository.removeByIndex(1)).thenReturn(null);
        assertThrows(ProjectNotFoundException.class, () -> projectService.removeProjectByIndex(params));
    }

    @Test
    void removeProjectByIndexAccessibleNot() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.USER);
        userService.setAppUser(user);

        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_INDEX, "1");
        when(mockRepository.findByIndex(1)).thenReturn(expected);
        assertThrows(AccessDeniedException.class, () -> projectService.removeProjectByIndex(params));
    }


    @Test
    void updateProjectByIdCharId() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_ID, "AA");
        params.put(PROJECT_NAME, "TEST");
        params.put("projectDescription", "TEST");
        assertThrows(IllegalArgumentException.class, () -> projectService.updateProjectById(params));
    }

    @Test
    void updateProjectByIdNotFound() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_ID, "1");
        params.put(PROJECT_NAME, "TEST");
        params.put("projectDescription", "TEST");
        when(mockRepository.update(1L, "TEST", "TEST")).thenReturn(null);
        assertThrows(ProjectNotFoundException.class, () -> projectService.updateProjectById(params));
    }

    @Test
    void updateProjectByIdGood() throws ProjectNotFoundException {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_ID, "1");
        params.put(PROJECT_NAME, "TEST");
        params.put("projectDescription", "TEST");
        when(mockRepository.update(1L, "TEST", "TEST")).thenReturn(expected);
        projectService.updateProjectById(params);
        verify(mockRepository, times(1)).update(1L, "TEST", "TEST");
    }


    @Test
    void updateProjectByIndexCharId() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_INDEX, "AA");
        params.put(PROJECT_NAME, "TEST");
        params.put("projectDescription", "TEST");
        assertThrows(IllegalArgumentException.class, () -> projectService.updateProjectByIndex(params));
    }

    @Test
    void updateProjectByIndexNotFound() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_INDEX, "1");
        params.put(PROJECT_NAME, "TEST");
        params.put("projectDescription", "TEST");
        when(mockRepository.findByIndex(1)).thenReturn(null);
        assertThrows(ProjectNotFoundException.class, () -> projectService.updateProjectByIndex(params));
    }

    @Test
    void updateProjectByIndexGood() throws ProjectNotFoundException {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_ID, "1");
        params.put(PROJECT_INDEX, "1");
        params.put(PROJECT_NAME, "TEST");
        params.put("projectDescription", "TEST");
        when(mockRepository.findByIndex(0)).thenReturn(expected);
        when(mockRepository.update(expected.getId(), "TEST", "TEST")).thenReturn(expected);
        projectService.updateProjectByIndex(params);
        verify(mockRepository, times(1)).update(expected.getId(), "TEST", "TEST");
    }

    @Test
    void updateProjectByIndexNotFound2() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_ID, "1");
        params.put(PROJECT_INDEX, "1");
        params.put(PROJECT_NAME, "TEST");
        params.put("projectDescription", "TEST");
        when(mockRepository.findByIndex(0)).thenReturn(expected);
        when(mockRepository.update(expected.getId(), "TEST", "TEST")).thenReturn(null);
        assertThrows(ProjectNotFoundException.class, () -> projectService.updateProjectByIndex(params));

    }


    @Test
    void viewProjectByIndexCharIndex() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_INDEX, "AA");
        params.put(PROJECT_NAME, "TEST");
        params.put("projectDescription", "TEST");
        assertThrows(IllegalArgumentException.class, () -> projectService.viewProjectByIndex(params));
    }

    @Test
    void viewProjectByIndexGood() throws ProjectNotFoundException {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_INDEX, "1");
        params.put(PROJECT_NAME, "TEST");
        params.put("projectDescription", "TEST");
        when(mockRepository.findByIndex(0)).thenReturn(expected);
        projectService.viewProjectByIndex(params);
        verify(mockRepository, times(1)).findByIndex(0);
    }

    @Test
    void findProjectByIdNullParams() throws ProjectNotFoundException, AccessDeniedException {
        Project result = projectService.findProjectById(null);
        assertNull(result);
    }

    @Test
    void findProjectByIdNotFound() {
        when(mockRepository.findById(1L)).thenReturn(null);
        assertThrows(ProjectNotFoundException.class, () -> projectService.findProjectById(1L));
    }

    @Test
    void findProjectByIdNotAccess() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.USER);
        userService.setAppUser(user);

        when(mockRepository.findById(1L)).thenReturn(expected);
        assertThrows(AccessDeniedException.class, () -> projectService.findProjectById(1L));
    }

    @Test
    void findProjectByIdGood() throws AccessDeniedException, ProjectNotFoundException {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);

        when(mockRepository.findById(1L)).thenReturn(expected);
        Project result = projectService.findProjectById(1L);
        assertTrue(new ReflectionEquals(expected).matches(result));

    }


    @Test
    void createProjectEmptyName() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "");
        params.put("projectDescription", "TEST");
        assertThrows(IllegalArgumentException.class, () -> projectService.createProject(params));
    }

    @Test
    void createProjectNotCreate() {
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "TEST");
        params.put("projectDescription", "TEST");
        when(mockRepository.create("TEST", UUID.randomUUID())).thenReturn(null);
        assertThrows(IllegalArgumentException.class, () -> projectService.createProject(params));
    }

    @Test
    void createProjectGood() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);

        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "TEST");
        params.put("projectDescription", "TEST");
        when(mockRepository.create("TEST", user.getUserId())).thenReturn(expected);
        Project result = projectService.createProject(params);
        assertTrue(new ReflectionEquals(expected).matches(result));
    }


    @Test
    void clearProjectEmptyList() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.USER);
        userService.setAppUser(user);
        when(mockRepository.findByUserId(user.getUserId())).thenReturn(Collections.emptyList());
        assertThrows(IllegalArgumentException.class, () -> projectService.clearProject());
    }

    @Test
    void clearProjectUser() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.USER);
        userService.setAppUser(user);
        List<Project> list = new ArrayList<>();
        list.add(expected);

        when(mockRepository.findByUserId(user.getUserId())).thenReturn(list);
        projectService.clearProject();
        verify(mockRepository, times(1)).clearListProject(list);
    }

    @Test
    void clearProjectAdmin() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);
        List<Project> list = new ArrayList<>();
        list.add(expected);
        when(mockRepository.findByUserId(user.getUserId())).thenReturn(list);
        projectService.clearProject();
        verify(mockRepository, times(1)).clear();
    }

    @Test
    void listProject() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);
        List<Project> list = new ArrayList<>();
        list.add(expected);
        when(mockRepository.findAll()).thenReturn(list);
        projectService.listProject();
        verify(mockRepository, times(1)).findAll();

    }

    @Test
    void addTaskToProjectNotFound() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);
        when(mockRepository.findById(1L)).thenReturn(null);
        assertThrows(ProjectNotFoundException.class, () -> projectService.addTaskToProject(1L, 1L));
    }

    @Test
    void addTaskToProject() throws AccessDeniedException, ProjectNotFoundException {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);
        when(mockRepository.findById(1L)).thenReturn(expected);
        projectService.addTaskToProject(1L, 1L);
        verify(mockRepository, times(1)).findById(1L);
    }

    @Test
    void removeTaskFromProjectNotFound() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);
        when(mockRepository.findById(1L)).thenReturn(null);
        assertThrows(ProjectNotFoundException.class, () -> projectService.removeTaskFromProject(1L, 1L));
    }

    @Test
    void removeTaskFromProject() throws AccessDeniedException, ProjectNotFoundException {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);
        when(mockRepository.findById(1L)).thenReturn(expected);
        projectService.removeTaskFromProject(1L, 1L);
        verify(mockRepository, times(1)).findById(1L);

    }

    @Test
    void assignProjectToUserAsUser() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.USER);
        userService.setAppUser(user);
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "TEST");
        params.put(PROJECT_ID, "1");
        params.put("userId", user.getUserId().toString());
        params.put("projectDescription", "TEST");

        assertThrows(AccessDeniedException.class, () -> projectService.assignProjectToUser(params));

    }

    @Test
    void assignProjectToUserParseNumberException() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "TEST");
        params.put(PROJECT_ID, "AA");
        params.put("userId", user.getUserId().toString());
        params.put("projectDescription", "TEST");

        assertThrows(IllegalArgumentException.class, () -> projectService.assignProjectToUser(params));

    }

    @Test
    void assignProjectToUserUserIdNulls() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "TEST");
        params.put(PROJECT_ID, "1");
        params.put("userId", "");
        params.put("projectDescription", "TEST");

        assertThrows(IllegalArgumentException.class, () -> projectService.assignProjectToUser(params));

    }

    @Test
    void assignProjectToUserProjectNotFound() {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "TEST");
        params.put(PROJECT_ID, "1");
        params.put("userId", user.getUserId().toString());
        params.put("projectDescription", "TEST");
        when(mockRepository.updateProjectOwner(1L, user.getUserId())).thenReturn(null);

        assertThrows(ProjectNotFoundException.class, () -> projectService.assignProjectToUser(params));
    }

    @Test
    void assignProjectToUserGood() throws AccessDeniedException, ProjectNotFoundException {
        UserService userService = UserService.getInstance(new UserRepository());
        User user = new User();
        user.setUserRole(UserRole.ADMIN);
        userService.setAppUser(user);
        Map<String, String> params = new HashMap<>();
        params.put(PROJECT_NAME, "TEST");
        params.put(PROJECT_ID, "1");
        params.put("userId", user.getUserId().toString());
        params.put("projectDescription", "TEST");
        when(mockRepository.updateProjectOwner(1L, user.getUserId())).thenReturn(expected);
        projectService.assignProjectToUser(params);
        verify(mockRepository, times(1)).updateProjectOwner(1L, user.getUserId());
    }

}