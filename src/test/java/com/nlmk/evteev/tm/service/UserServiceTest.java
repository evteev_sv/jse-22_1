package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.User;
import com.nlmk.evteev.tm.enumerations.UserRole;
import com.nlmk.evteev.tm.repository.UserRepository;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


class UserServiceTest {

    private final String emptyString = "";
    private final String loginName = "TEST";
    private final String adminName = "ivanov";
    private final String password = "TEST_PWD";

    private static UserRepository userRepository;
    private UserService userService;

    @BeforeAll
    public static void initClass() {
        userRepository = Mockito.mock(UserRepository.class);
    }

    @BeforeEach
    public void init() {
        userService = UserService.getInstance(userRepository);
        //Mockito.reset(userRepository);
    }

    @Test
    void getInstance() {
        UserService service = UserService.getInstance(userRepository);
        UserService service1 = UserService.getInstance(userRepository);
        assertTrue(new ReflectionEquals(service).matches(service1));
    }

    @Test
    void createUserGood() {
        Map<String, String> params = new HashMap<>();
        params.put("loginName", loginName);
        params.put("password", password);
        params.put("userRoleAdmin", "n");

        User expected = new User();
        expected.setLoginName(loginName);
        expected.setPassword(password);
        expected.setUserRole(UserRole.USER);

        when(userRepository.create(any(), any(), any())).thenReturn(expected);

        Optional<User> result = userService.createUser(params);
        result.ifPresent(user -> assertTrue(new ReflectionEquals(expected).matches(user)));
    }

    @Test
    void createUserEmptyLogin() {
        Map<String, String> params = new HashMap<>();
        params.put("loginName", emptyString);
        params.put("password", password);
        params.put("userRoleAdmin", "n");

        Optional<User> result = userService.createUser(params);
        assertTrue(result.isEmpty());
    }

    @Test
    void createDefinedUser() {
        Map<String, String> params = new HashMap<>();
        params.put("loginName", adminName);
        params.put("password", password);
        params.put("userRoleAdmin", "y");

        User expected = new User();
        expected.setLoginName(adminName);
        expected.setPassword(password);
        expected.setUserRole(UserRole.ADMIN);

        when(userRepository.getUserByLoginName(adminName)).thenReturn(expected);

        Optional<User> result = userService.createUser(params);
        assertTrue(result.isEmpty());
    }

    @Test
    void createUserEmptyPassword() {
        Map<String, String> params = new HashMap<>();
        params.put("loginName", loginName);
        params.put("password", emptyString);
        params.put("userRoleAdmin", "n");

        Optional<User> result = userService.createUser(params);
        assertTrue(result.isEmpty());
    }

    @Test
    void updateUserNotFound() {
        Map<String, String> params = new HashMap<>();
        params.put("loginName", emptyString);
        params.put("password", emptyString);
        params.put("userRoleAdmin", "n");
        when(userRepository.getUserByLoginName(emptyString)).thenReturn(null);
        assertThrows(IllegalArgumentException.class, () -> userService.updateUser(params));
    }

    @Test
    void updateUserEmptyPassword() {
        Map<String, String> params = new HashMap<>();
        params.put("loginName", loginName);
        params.put("password", emptyString);
        params.put("userRoleAdmin", "n");
        params.put("firstName", "");
        params.put("middleName", "");
        params.put("secondName", "");

        User expected = new User();
        expected.setLoginName(loginName);
        expected.setPassword(emptyString);
        expected.setUserRole(UserRole.USER);

        when(userRepository.getUserByLoginName(loginName)).thenReturn(expected);

        assertThrows(IllegalArgumentException.class, () -> userService.updateUser(params));
    }

    @Test
    void updateUser() {
        Map<String, String> params = new HashMap<>();
        params.put("loginName", adminName);
        params.put("password", password);
        params.put("userRoleAdmin", "n");
        params.put("firstName", "");
        params.put("middleName", "");
        params.put("secondName", "");

        User expected = new User();
        expected.setLoginName(adminName);
        expected.setPassword(emptyString);
        expected.setUserRole(UserRole.USER);
        expected.setFirstName("");
        expected.setMiddleName("");
        expected.setSecondName("");

        userService.userList.add(expected);
        when(userRepository.getUserByLoginName(adminName)).thenReturn(expected);
        when(userRepository.update(adminName, TaskManagerUtil.getMD5Hash(password),
                UserRole.USER, emptyString, emptyString, emptyString)).thenReturn(expected);

        Optional<User> result = userService.updateUser(params);
        result.ifPresent(user -> assertTrue(new ReflectionEquals(expected).matches(user)));
    }

    @Test
    void deleteUserNoAdminPrivs() {
        User expected = new User();
        expected.setLoginName(loginName);
        expected.setPassword(emptyString);
        expected.setUserRole(UserRole.USER);
        userService.setAppUser(expected);
        Map<String, String> params = new HashMap<>();
        params.put("loginName", loginName);
        Optional<User> result = userService.deleteUser(params);
        assertTrue(result.isEmpty());
    }

    @Test
    void deleteUserWithAdminPrivs() {
        User expected = new User();
        expected.setLoginName(adminName);
        expected.setPassword(emptyString);
        expected.setUserRole(UserRole.ADMIN);
        userService.setAppUser(expected);

        Map<String, String> params = new HashMap<>();
        params.put("loginName", loginName);

        when(userRepository.deleteUserByLogin(loginName)).thenReturn(expected);

        Optional<User> result = userService.deleteUser(params);
        result.ifPresent(user -> assertTrue(new ReflectionEquals(expected).matches(user)));
    }

    @Test
    void deleteUserWithEmptyLogin() {
        User expected = new User();
        expected.setLoginName(adminName);
        expected.setPassword(emptyString);
        expected.setUserRole(UserRole.ADMIN);
        userService.setAppUser(expected);

        Map<String, String> params = new HashMap<>();
        params.put("loginName", emptyString);

        Optional<User> result = userService.deleteUser(params);
        assertTrue(result.isEmpty());
    }

    @Test
    void userLogin() {
        Map<String, String> params = new HashMap<>();
        params.put("loginName", adminName);
        params.put("password", password);
        params.put("userRoleAdmin", "n");
        params.put("firstName", "");
        params.put("middleName", "");
        params.put("secondName", "");

        User expected = new User();
        expected.setLoginName(adminName);
        expected.setPassword(password);
        expected.setUserRole(UserRole.USER);
        expected.setFirstName("");
        expected.setMiddleName("");
        expected.setSecondName("");

        userService.userList.add(expected);
        when(userRepository.getUserByLoginName(adminName)).thenReturn(expected);

        userService.userLogin(params);
        assertTrue(new ReflectionEquals(expected).matches(userService.getAppUser()));
    }

    @Test
    void viewUserById() {
        List<User> expected = new ArrayList<>();
        User expectedUser = new User();
        expectedUser.setLoginName(loginName);
        expectedUser.setPassword(password);
        expectedUser.setUserRole(UserRole.USER);
        expected.add(expectedUser);

        when(userRepository.findAll()).thenReturn(expected);
        userService.viewUserById(UUID.randomUUID().toString());
        List<User> result = userRepository.findAll();
        assertTrue(new ReflectionEquals(expected).matches(result));
    }

    @Test
    void viewUserByIdNotFound() {
        User expectedUser = new User();
        expectedUser.setLoginName(loginName);
        expectedUser.setPassword(password);
        expectedUser.setUserRole(UserRole.USER);
        when(userRepository.findAll()).thenReturn(Collections.emptyList());
        userService.viewUserById(UUID.randomUUID().toString());
        List<User> result = userRepository.findAll();
        assertTrue(result.isEmpty());
    }

    @Test
    void viewUserByIdGood() {
        List<User> expected = new ArrayList<>();
        User expectedUser = new User();
        expectedUser.setLoginName(loginName);
        expectedUser.setPassword(password);
        expectedUser.setUserRole(UserRole.USER);
        expected.add(expectedUser);

        when(userRepository.findAll()).thenReturn(expected);

        userService.viewUserById(UUID.randomUUID().toString());
        List<User> result = userRepository.findAll();
        assertTrue(new ReflectionEquals(expected.get(0)).matches(result.get(0)));
    }

}